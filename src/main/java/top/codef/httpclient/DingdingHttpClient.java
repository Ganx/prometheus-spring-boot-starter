package top.codef.httpclient;

import top.codef.pojos.dingding.DingDingNotice;
import top.codef.pojos.dingding.DingDingResult;
import top.codef.properties.notice.DingDingNoticeProperty;

@FunctionalInterface
public interface DingdingHttpClient {

	DingDingResult doSend(DingDingNotice notice, DingDingNoticeProperty dingDingNoticeProperty);

}
